// *************************************************************************************************************
// *************************************************************************************************************
//           Conor COSTELLO, Rayan ELLEUCH - Marché
// *************************************************************************************************************
// *************************************************************************************************************


/* ------------------------------------------------

MAIN TAGS:
	KEY		reference to the attribute	mandatory
	VAL		content of the attribute	mandatory
	CAT		category of attribute		default = INFO
	TYPE	type of data in VAL			default = STR

CATS:
	INFO	static information -- default
	VAR		dynamic information
	ACT		static action 
	PRO		static process  -- nyi TODO
	REL	    dynamic pointer to another topic

TYPES:		javascript types of tag VAL
	STR		string or array of strings -- default
	INT		integer or array of integer
	BOOL	boolean
	EXPR	code wrapped into a string

SPECIFIC TAGS:	WHY, EFFECT, REVERSE, UNDO

------------------------------------------------ */

// ====================================================================
//                       MODEL TOPICS DESCRIPTION
// ====================================================================


// ======================  TOPICS VENDEURS  ======================
var vendeurTopic_Theo = [
	[["KEY", "_class"],						["VAL", "bot"], ["BOT","vendeurBot1"]],
	[["KEY", "_reference"],					["VAL", ["first","premier","theo","crick"]]],
	[["KEY", "_charprefix"],				["VAL", "theo"]],
	[["KEY", "_read"],						["VAL", ["vendeurTopic_Theo", "userTopic","tomato_Topic_Theo", "onion_Topic_Theo", "potato_Topic_Theo"]]],
	[["KEY", "_write"],						["VAL", ["userTopic","tomato_Topic_Theo", "onion_Topic_Theo", "potato_Topic_Theo"]]],
	[["KEY", "_exec"],						["VAL", ["tomato_Topic_Theo","onion_Topic_Theo","potato_Topic_Theo"]]],
	[["KEY", "type"],						["VAL", ["human","male"]]],
	[["KEY", "name"],						["VAL", "Theo"],
											["WHY","Because"]],
	[["KEY", "age"],						["VAL", 20],["TYPE","INT"],
											["ONASK","20 years old"], 
											["WHY","Because"]],
	[["KEY", "gender"],						["VAL", "male"],
											["ONASK", "I'm but a humble man" ]],
	[["KEY", ["job"]],			          	["VAL", "Ba je vend des légumes"]],
	[["KEY", ["job","phone"]],				["VAL", "06 marché"],
											["TYPE","INT"]],
	[["KEY", ["home","location"]],		    ["VAL", "Paris, 20éme"]],
	[["KEY", "date"],						["VAL", "I'm not sure, I think the 20th"]],
	// REL
	[["KEY", "relative"],					["VAL", []],
											["ONASK", BOT_printRelativeList]],
	// FEELINGS
	[["KEY", "happiness"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]], // 7 standard feelings
	[["KEY", "confidence"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "irritability"],	["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "satisfaction"],	["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "respect"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "force"],			["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "excitement"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]]
];

var vendeurTopic_Josette = [
	[["KEY", "_class"],						["VAL", "bot"], ["BOT","vendeurBot2"]],
	[["KEY", "_reference"],					["VAL", ["second","deuxieme","coco","josette"]]],
	[["KEY", "_charprefix"],				["VAL", "josette"]],
	[["KEY", "_read"],						["VAL", ["vendeurTopic_Josette", "userTopic","tomato_Topic_Josette", "onion_Topic_Josette", "potato_Topic_Josette"]]],
	[["KEY", "_write"],						["VAL", ["userTopic"]]],
	[["KEY", "_exec"],						["VAL", ["tomato_Topic_Josette","onion_Topic_Josette","potato_Topic_Josette"]]],
	[["KEY", "type"],						["VAL", ["human","female"]]],
	[["KEY", "name"],						["VAL", "Josette"],
											["WHY","Everyone has a name"]],
	[["KEY", "age"],						["VAL", 40],["TYPE","INT"],
											["ONASK","Older than you ;)"], 
											["WHY","Time time time"]],
	[["KEY", "gender"],						["VAL", "female"],
											["ONASK", "A woman but a strong woman" ]],
	[["KEY", "day"],						["VAL", "pleasant"],
											["ONASK", "You're right, it's a very nice day"]],									
	[["KEY", ["job"]],			          	["VAL", "Selling nice veg"]],
	[["KEY", ["job","phone"]],				["VAL", "06 marché"],["TYPE","INT"]],
	[["KEY", ["home","location"]],		    ["VAL", "Paris, 18éme"]],
	[["KEY", "date"],						["VAL", "It's the 20th"]],
	// REL
	[["KEY", "relative"],					["VAL", []],
											["ONASK", BOT_printRelativeList]],
	// FEELINGS
	//when josette is happy the function will execute
	[["KEY", "happiness"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]], // 7 standard feelings
	[["KEY", "confidence"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "irritability"],	["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "satisfaction"],	["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "respect"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "force"],			["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "excitement"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]]
];

var vendeurTopic_Jacky = [
	[["KEY", "_class"],						["VAL", "bot"], ["BOT","vendeurBot3"]],
	[["KEY", "_reference"],					["VAL", ["third","troisieme","jacky","watson"]]],
	[["KEY", "_charprefix"],				["VAL", "jacky"]],
	[["KEY", "_read"],						["VAL", ["vendeurTopic_Jacky", "userTopic","tomato_Topic_Jacky", "onion_Topic_Jacky", "potato_Topic_Jacky"]]],
	[["KEY", "_write"],						["VAL", ["userTopic"]]],
	[["KEY", "_exec"],						["VAL", ["tomato_Topic_Jacky", "onion_Topic_Jacky", "potato_Topic_Jacky"]]],
	[["KEY", "type"],						["VAL", ["human","male"]]],
	[["KEY", "name"],						["VAL", "Jacky"],
											["WHY","It is what it is"]],
	[["KEY", "age"],						["VAL", 60],["TYPE","INT"],
											["ONASK","Old enough"], 
											["WHY","It happens"]],
	[["KEY", "gender"],						["VAL", "male"],
											["ONASK", "I'm a proud man" ]],
	[["KEY", ["job"]],			          	["VAL", "Selling veg"]],
	[["KEY", ["job","phone"]],				["VAL", "06 marché"],["TYPE","INT"]],
	[["KEY", ["home","location"]],		    ["VAL", "Paris, 2éme"]],
	[["KEY", "date"],						["VAL", "21st maybe, am not sure"]],
	// REL
	[["KEY", "relative"],					["VAL", []],
											["ONASK", BOT_printRelativeList]],
	// FEELINGS
	[["KEY", "happiness"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]], // 7 standard feelings
	[["KEY", "confidence"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "irritability"],	["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "satisfaction"],	["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "respect"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "force"],			["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "excitement"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]]
];

	
// ======================  TOPIC LÉGUMES  ======================

//oui c'est pas un légume mais ça va. 
var tomato_Topic_Theo = [
	// INFO 
	[["KEY", "_class"],			["VAL", "tomato"]],
	[["KEY", "_reference"],		["VAL", ["tomato","tomate","tomatoes", "red"]]],
	[["KEY", "type"],			["VAL", ["légume"]]],
	// VAR 
	[["KEY", "name"],			["VAL", "Tomato"], ["CAT","VAR"]],
	[["KEY", "price"],			["VAL", 2.00], ["CAT","VAR"], ["TYPE","INT"],
								["ONASK", function(s){return "The price of tomatoes is " + s + " euro"}]],
	[["KEY", "colour"],			["VAL", "red"],	["CAT","VAR"]],
	
	[["KEY", "action"],			["VAL", ["sell"]],
								["ONASK",BOT_printActionList]],
	[["KEY", "sell"],			["VAL", "func_sellTomato"],	["CAT","ACT"],
								["EFFECT", "Buys a tomato"]]
];
var tomato_Topic_Josette = [
	// INFO 
	[["KEY", "_class"],			["VAL", "tomato"]],
	[["KEY", "_reference"],		["VAL", ["tomato","tomate","tomatoes", "red"]]],
	[["KEY", "type"],			["VAL", ["légume"]]],
	// VAR 
	[["KEY", "name"],			["VAL", "Tomato"], ["CAT","VAR"]],
	[["KEY", "price"],			["VAL", 3.00],	["CAT","VAR"], ["TYPE","INT"],
								["ONASK", function(s){return "The price of tomatoes is " + s + " euro"}]],
	[["KEY", "colour"],			["VAL", "red"],	["CAT","VAR"]],
	
	[["KEY", "action"],			["VAL", ["sell"]],
								["ONASK",BOT_printActionList]],
	[["KEY", "sell"],			["VAL", "func_sellTomato"],	["CAT","ACT"],
								["EFFECT", "Buys a tomato"]]
];
var tomato_Topic_Jacky = [
	// INFO 
	[["KEY", "_class"],			["VAL", "tomato"]],
	[["KEY", "_reference"],		["VAL", ["tomato","tomate","tomatoes", "red"]]],
	[["KEY", "type"],			["VAL", ["légume"]]],
	// VAR 
	[["KEY", "name"],			["VAL", "Tomato"], ["CAT","VAR"]],
	[["KEY", "price"],			["VAL", 2.00],	["CAT","VAR"], ["TYPE","INT"],
								["ONASK", function(s){return "The price of tomatoes is " + s + " euro"}]],
	[["KEY", "colour"],			["VAL", "red"],	["CAT","VAR"]],
	
	[["KEY", "action"],			["VAL", ["sell"]],
								["ONASK",BOT_printActionList]],
	[["KEY", "sell"],			["VAL", "func_sellTomato"],	["CAT","ACT"],
								["EFFECT", "Buys a tomato"]]
];

var onion_Topic_Theo = [
	// INFO 
	[["KEY", "_class"],			["VAL", "onion"]],
	[["KEY", "_reference"],		["VAL", ["onion","onions","oignon", "cry", "pleur"]]],
	[["KEY", "type"],			["VAL", ["légume"]]],
	// VAR 
	[["KEY", "name"],			["VAL", "Onion"], ["CAT","VAR"]],
	[["KEY", "price"],			["VAL", 3.00],	["CAT","VAR"], ["TYPE","INT"],
								["ONASK", function(s){return "The price of onions is " + s + " euro"}]],
	[["KEY", "colour"],			["VAL", "violet"],	["CAT","VAR"]],
	
	[["KEY", "action"],			["VAL", ["sell"]],
								["ONASK",BOT_printActionList]],
	[["KEY", "sell"],			["VAL", "func_sellOnion"],	["CAT","ACT"],
								["EFFECT", "Buys an onion"]]
];
var onion_Topic_Josette = [
	// INFO 
	[["KEY", "_class"],			["VAL", "onion"]],
	[["KEY", "_reference"],		["VAL", ["onion","onions","oignon", "cry", "pleur"]]],
	[["KEY", "type"],			["VAL", ["légume"]]],
	// VAR 
	[["KEY", "name"],			["VAL", "Onion"], ["CAT","VAR"]],
	[["KEY", "price"],			["VAL", 2.00],	["CAT","VAR"], ["TYPE","INT"],
								["ONASK", function(s){return "The price of onions is " + s + " euro"}]],
	[["KEY", "couleur"],		["VAL", "violet"],	["CAT","VAR"]],
	
	[["KEY", "action"],			["VAL", ["sell"]],
								["ONASK",BOT_printActionList]],
	[["KEY", "sell"],			["VAL", "func_sellOnion"],	["CAT","ACT"],
								["EFFECT", "Buys an onion"]]
];
var onion_Topic_Jacky = [
	// INFO 
	[["KEY", "_class"],			["VAL", "onion"]],
	[["KEY", "_reference"],		["VAL", ["onion","onions","oignon", "cry", "pleur"]]],
	[["KEY", "type"],			["VAL", ["légume"]]],
	// VAR 
	[["KEY", "name"],			["VAL", "Onion"], ["CAT","VAR"]],
	[["KEY", "price"],			["VAL", 2.00],	["CAT","VAR"], ["TYPE","INT"],
								["ONASK", function(s){return "The price of onions is " + s + " euro"}]],
	[["KEY", "colour"],			["VAL", "violet"],	["CAT","VAR"]],
	
	[["KEY", "action"],			["VAL", ["sell"]],
								["ONASK",BOT_printActionList]],
	[["KEY", "sell"],			["VAL", "func_sellOnion"],	["CAT","ACT"],
								["EFFECT", "Buys an onion"]]
];

var potato_Topic_Theo = [
	// INFO 
	[["KEY", "_class"],			["VAL", "potato"]],
	[["KEY", "_reference"],		["VAL", ["potato", "potatoes", "pomme de terre", "pommes de terre", "frites", "chips", "fries", "patate", "spuds"]]],
	[["KEY", "type"],			["VAL", ["légume"]]],
	// VAR 
	[["KEY", "name"],			["VAL", "Potato"], ["CAT","VAR"]],
	[["KEY", "price"],			["VAL", 2.00],	["CAT","VAR"], ["TYPE","INT"],
								["ONASK", function(s){return "The price of potato is " + s + " euro"}]],
	[["KEY", "colour"],			["VAL", "red"],	["CAT","VAR"]],
	
	[["KEY", "action"],			["VAL", ["sell"]],
								["ONASK",BOT_printActionList]],
	[["KEY", "sell"],			["VAL", "func_sellPotato"],	["CAT","ACT"],
								["EFFECT", "Buys an potato"]]
];
var potato_Topic_Josette = [
	// INFO 
	[["KEY", "_class"],			["VAL", "potato"]],
	[["KEY", "_reference"],		["VAL", ["potato", "potatoes", "pomme de terre", "pommes de terre", "frites", "chips", "fries", "patate", "spuds"]]],
	[["KEY", "type"],			["VAL", ["légume"]]],
	// VAR 
	[["KEY", "name"],			["VAL", "Potato"], ["CAT","VAR"]],
	[["KEY", "price"],			["VAL", 2.00],	["CAT","VAR"], ["TYPE","INT"],
								["ONASK", function(s){return "The price of potato is " + s + " euro"}]],
	[["KEY", "colour"],			["VAL", "red"],	["CAT","VAR"]],
	
	[["KEY", "action"],			["VAL", ["sell"]],
								["ONASK",BOT_printActionList]],
	[["KEY", "sell"],			["VAL", "func_sellPotato"],	["CAT","ACT"],
								["EFFECT", "Buys an potato"]]
];
var potato_Topic_Jacky = [
	// INFO 
	[["KEY", "_class"],			["VAL", "potato"]],
	[["KEY", "_reference"],		["VAL", ["potato", "potatoes", "pomme de terre", "pommes de terre", "frites", "chips", "fries", "patate", "spuds"]]],
	[["KEY", "type"],			["VAL", ["légume"]]],
	// VAR 
	[["KEY", "name"],			["VAL", "Potato"], ["CAT","VAR"]],
	[["KEY", "price"],			["VAL", 2.00],	["CAT","VAR"], ["TYPE","INT"],
								["ONASK", function(s){return "The price of potato is " + s + " euro"}]],
	[["KEY", "colour"],			["VAL", "red"],	["CAT","VAR"]],
	
	[["KEY", "action"],			["VAL", ["sell"]],
								["ONASK",BOT_printActionList]],
	[["KEY", "sell"],			["VAL", "func_sellPotato"],	["CAT","ACT"],
								["EFFECT", "Buys an potato"]]
];


// =======================  TOPIC USER  ========================
var userTopic = [
	// INFO 
	[["KEY", "_class"],			["VAL", "user"]],
	[["KEY", "_reference"],		["VAL", ["moi","me","user"]]],
	[["KEY", "type"],			["VAL", ["person"]]],
	// VAR 
	[["KEY", "name"],			["VAL", "User"], ["CAT","VAR"],
								["WHY", "Je ne sais pas"]
								],
	[["KEY", "age"],			["VAL", "unknown"],	["CAT","VAR"]],
	[["KEY", "gender"],			["VAL", "unknown"],	["CAT","VAR"]],
	[["KEY", "job"],			["VAL", "unknown"],	["CAT","VAR"]],
	
	//money spent
	[["KEY", "money"],			["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "tomato_quantity"],["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "potato_quantity"],["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "onion_quantity"], ["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],

	// OPINIONS
	[["KEY", "judgement"],		["VAL", []], ["CAT","VAR"], ["ONASK",BOT_printJudgementList]], // 6 standard opinions 
	[["KEY", "preference"],		["VAL", []], ["CAT","VAR"], ["ONASK",BOT_printPreferenceList]], 
	[["KEY", "distaste"],		["VAL", []], ["CAT","VAR"], ["ONASK",BOT_printDistasteList]], 
	[["KEY", "suggestion"],		["VAL", []], ["CAT","VAR"], ["ONASK",BOT_printSuggestionList]],  
	[["KEY", "objection"],		["VAL", []], ["CAT","VAR"], ["ONASK",BOT_printObjectionList]],  
	[["KEY", "intention"],		["VAL", []], ["CAT","VAR"], ["ONASK",BOT_printIntentionList]],  
	// FEELINGS
	[["KEY", "happiness"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]], // 7 standard feelings
	[["KEY", "confidence"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "irritability"],	["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "satisfaction"],	["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "respect"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "force"],			["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	[["KEY", "excitement"],		["VAL", 0], ["CAT","VAR"], ["TYPE","INT"]],
	// REL
	[["KEY", "relative"],		["VAL", []]] // none
];

// =========  Initialization of bots and declaration of topics  ==========
var vendeurBot1 = new BOT_makeBot("vendeurBot1","vendeurTopic_Theo");
var vendeurBot2  = new BOT_makeBot("vendeurBot2","vendeurTopic_Josette");
var vendeurBot3  = new BOT_makeBot("vendeurBot3","vendeurTopic_Jacky");

BOT_declareTopics(["userTopic", "tomato_Topic_Theo","tomato_Topic_Josette","tomato_Topic_Jacky", 
								"onion_Topic_Theo", "onion_Topic_Josette", "onion_Topic_Jacky", 
								"potato_Topic_Theo","potato_Topic_Josette","potato_Topic_Jacky"]); 

BOT_theBotId		= "vendeurBot1";		// sets current bot id 
BOT_theTopicId		= "vendeurTopic_Theo";		// sets current topic id
BOT_theUserTopicId	= "userTopic";		// sets topic of current user id

 
// *************************************************************************************************************
// *************************************************************************************************************
//                                        SPECIFIC APPLICATION FUNCTIONS
// *************************************************************************************************************
// *************************************************************************************************************


function setTopicBackground(topicId, selected){
	
	//make sure it is not selected if false
	var topic = topicId.substring(0,6);
	
	if (topic.indexOf("onion") > -1){
		topic = "onion"
	}
	
	if (!selected){
		if (!(BOT_theTopicId.indexOf(topic) > -1)){
			//this topic is not active but the image needs
			//to be kept highlighted
			$("#" + topic + "Topic_img").css("background-color", "#fff");
		}
		return;
	} 
	
	$("#" + topic + "Topic_img").css("background-color", "#fffb72");
}

function updateUserMoneyLabel(){
	$("#userMoney").html("&euro; " + BOT_get("userTopic","money","VAL"));
}


// ====================================================================
//                  INTERNAL FUNCTIONS OF MODEL
// ====================================================================

function func_sellTomato(topic) {
	//sell a tomato to the user
	var old = BOT_get(BOT_theUserTopicId,"tomato_quantity","VAL");
	BOT_set(BOT_theUserTopicId,"tomato_quantity","VAL", old + 1);
	
	//update user money
	old = BOT_get(BOT_theUserTopicId,"money","VAL");
	BOT_set(BOT_theUserTopicId,"money","VAL", old + BOT_get(topic,"price","VAL"));
}

function func_sellOnion(topic) {
	//sell a tomato to the user
	var old = BOT_get(BOT_theUserTopicId,"onion_quantity","VAL");
	BOT_set(BOT_theUserTopicId,"onion_quantity","VAL", old + 1);
	
	//update user money
	old = BOT_get(BOT_theUserTopicId,"money","VAL");
	BOT_set(BOT_theUserTopicId,"money","VAL", old + BOT_get(topic,"price","VAL"));
}

function func_sellPotato(topic) {
	//sell a tomato to the user
	var old = BOT_get(BOT_theUserTopicId,"potato_quantity","VAL");
	BOT_set(BOT_theUserTopicId,"potato_quantity","VAL", old + 1);
	
	//update user money
	old = BOT_get(BOT_theUserTopicId,"money","VAL");
	BOT_set(BOT_theUserTopicId,"money","VAL", old + BOT_get(topic,"price","VAL"));
}

function func_calculate_new_tomato_price_Theo(botTopic) { 

	//each time nice down 10%
	var price = BOT_get("tomato_Topic_Theo","price","VAL");
	
	if (price < 1.5){
		price = 1.5;
		BOT_reqAnswerLong += ". $1.50 is the lowest I can go for my tomatoes";
	}
	
	BOT_set("tomato_Topic_Theo","price","VAL", price);
}

function func_calculate_new_tomato_price_Josette(botTopic) {
	
	//each time ask down 10%
	var price = BOT_get("tomato_Topic_Josette","price","VAL");

	price = price - (price * 0.1);
	
	if (price < 1){
		price = 1;
	} else {
		BOT_reqAnswerLong += ". You make me happy, I will reduce the price of my tomatoes.";
	}
	
	BOT_set("tomato_Topic_Josette","price","VAL", price);
}

function func_calculate_new_onion_price_Theo(botTopic) { 
	
	var price = BOT_get("onion_Topic_Theo","price","VAL");
	
	if (price < 1){
		price = 1;
		BOT_reqAnswerLong += ". $1 is the lowest I can go for my onions";
	}
	
	BOT_set("onion_Topic_Theo","price","VAL", price);
}

function func_calculate_new_potato_price_Theo(botTopic) { 
	//each time nice down 10%
	var price = BOT_get("potato_Topic_Theo","price","VAL");
	
	if (price < 1.5){
		price = 1.5;
		BOT_reqAnswerLong += ". $1.50 is the lowest I can go for my potatos";
	}
	
	BOT_set("potato_Topic_Theo","price","VAL", price);
}

function func_calculate_new_tomato_price_Jacky(botTopic) { 
	//each time return
	var price = BOT_get("tomato_Topic_Jacky","price","VAL");
	
	price = price - (price * 0.1);
	
	if (price < 1.5){
		price = 1.5;
	} else {
		BOT_reqAnswerLong += ". You make me happy, I will reduce the price of my tomatoes.";
	}
	
	BOT_set("tomato_Topic_Jacky","price","VAL", price);
}

function func_calculate_new_potato_price_Jacky(botTopic) { 
	//each time return
	var price = BOT_get("potato_Topic_Jacky","price","VAL");
	
	price = price - (price * 0.1);
	
	if (price < 1){
		price = 1;
	} else {
		BOT_reqAnswerLong += ". Please check out my onions, I have reduced the price."
	}
	
	BOT_set("potato_Topic_Jacky","price","VAL", price);
}

//score
function calculateScore() {
	
	//best price is 5 of each veg at $1 per veg
	var bestPrice = 15;
	var reqQuantity = 15;
	
	var qt = BOT_get(BOT_theUserTopicId,"tomato_quantity","VAL");
	var qo = BOT_get(BOT_theUserTopicId,"onion_quantity","VAL");
	var qp = BOT_get(BOT_theUserTopicId,"potato_quantity","VAL");
	
	//user needs to buy minimum 15 veg
	if ((qt + qo + qp) < reqQuantity){
		return;
	}
	
	var money = BOT_get(BOT_theUserTopicId,"money","VAL");
	
	//best score out of 100
	score = (bestPrice / money) * 100;
	
	$("#submitPanel").addClass("text-center");
	$("#submitPanel").html("<span class=\"label label-default\" style=\"font-size:x-large;\">Score: " + score + " / 100</span>");
}

//legume process function
function func_happy(botTopic, topic) { // topic is topic object
	if(botTopic == "vendeurTopic_Josette"){
		func_calculate_new_tomato_price_Josette(botTopic);
	}
}

function func_greet(botTopic, topic){
	if(botTopic == "vendeurTopic_Jacky"){
		func_calculate_new_potato_price_Jacky(botTopic);
	}
}

// ====================================================================
//        EVENTS  HANDLERS & REQUESTS SPECIFIC POSTPROCESSING 
// ====================================================================

function BOT_onSwitchBot(oldbotid,newbotid) {
	//afficher l'agent
	$("#" + oldbotid + "_img").removeClass("show");
	$("#" + oldbotid + "_img").addClass("hidden");
	$("#" + newbotid + "_img").removeClass("hidden");
	$("#" + newbotid + "_img").addClass("show");
}

function updateValues(topic) {
	if(BOT_reqEmote == "HAPPY"){
		func_happy(eval(BOT_theBotId).topicId, topic);
	}
	if(BOT_reqEmote == "SORRY"){
		//to something
	}
	if(BOT_reqEmote == "HELLO"){
		func_greet(eval(BOT_theBotId).topicId, topic);
	}
	
	//update each turn for theo
	if (eval(BOT_theBotId).topicId == "vendeurTopic_Theo"){
		func_calculate_new_tomato_price_Theo();
		func_calculate_new_onion_price_Theo();
		func_calculate_new_potato_price_Theo();
	}
}

function updateUI() {
	var bot = eval(BOT_theBotId);
	var bn  = BOT_topicToName(bot.topicId);
	// TOPIC SELECTION
	for ( var i in BOT_topicIdList)  {
		var tid = BOT_topicIdList[i];
		var tn  = BOT_get(tid,"name", "VAL")
		var tc  = BOT_get(tid,"_class","VAL")
		if(tn != undefined)  {
			if(BOT_theTopicId == tid) {
				//yellow border
				setTopicBackground(tid, true);
			}
			else {
				//white background
				setTopicBackground(tid, false);
			}
		}
	}
	
	updateUserMoneyLabel();
	calculateScore();
}

// ====================================================================
//        CORPUS TEXT TO BE EVALUATED 
// ====================================================================

function evaluateExpression(phrase){
	if(phrase == null || phrase == ""){
		return;
	}
	
	phrase = phrase.toLowerCase();
	
	if (phrase.indexOf("how much is") > -1){
		phrase = phrase.replace("how much is", "ask price");
	}
	else if (phrase.indexOf("how much are") > -1){
		phrase = phrase.replace("how much are", "ask price");
	}
	if (phrase.indexOf("i would like") > -1){
		phrase = phrase.replace("i would like", "want");
	}
	if (phrase.indexOf("i want") > -1){
		phrase = phrase.replace("i want", "want");
	}
	if (phrase.indexOf("want") > -1){
		phrase = phrase.replace("want", "sell");
	}
	else if (phrase.indexOf("buy") > -1){
		phrase = phrase.replace("buy", "sell");
	}
	if (phrase.indexOf("give me") > -1){
		phrase = phrase.replace("give me", "sell");
	}
	
	//if (phrase.indexOf("i want") > -1){
	//	phrase = phrase.replace("i want", "x");
	//}
	//in this context how much means ask
	if (phrase.indexOf("value") > -1){
		if(phrase.indexOf("how") > -1){
			phrase = phrase.replace("how", "ask");
		}
		
		/*
		if (phrase.indexOf("tomatoes") > -1){
			phrase = phrase.replace("tomatoes", "tomato price");
		}
		else if (phrase.indexOf("tomato") > -1){
			phrase = phrase.replace("tomato", "tomato price");
		}
		if (phrase.indexOf("onions") > -1){
			phrase = phrase.replace("onions", "onion price");
		}
		else if (phrase.indexOf("onion") > -1){
			phrase = phrase.replace("onion", "tomato price");
		}
		if (phrase.indexOf("potatoes") > -1){
			phrase = phrase.replace("potatoes", "potato price");
		}
		else if (phrase.indexOf("potato") > -1){
			phrase = phrase.replace("potato", "potato price");
		}
		*/
	}
	
	return phrase;
}

// *************************************************************************************************************
// *************************************************************************************************************
//                                                END OF THE CODE
// *************************************************************************************************************
// *************************************************************************************************************
 
 
 
 
 
 
 
 
 
 











